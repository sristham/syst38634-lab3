package password;

/**
 * 
 * @author Jonathan Sriskandarajah 991531114
 *
 */

public class PasswordValidator {

	private static int MIN_LENGTH = 8;

	/**
	 * 
	 * @param password
	 * @return true if the number of characters in the password is 8 or more. No spaces are allowed
	 */

	public static boolean isValidLength(String password) {
		return password != null && password.trim().length() >= MIN_LENGTH;
	}

	/**
	 * 
	 * @param password
	 * @return true if password has at least 2 digits
	 */
	public static boolean hasValidDigits(String password) {
		return password != null && password.matches("([^0-9]*[0-9]){2}.*");
	}

	/**
	 * 
	 * @param password
	 * @return true if password has uppercase and lowercase letters
	 */

	public static boolean hasValidCaseCharacters(String password) {
		return password != null && password.matches("^.*[a-z].*$") && password.matches("^.*[A-Z].*$");
	}
}
